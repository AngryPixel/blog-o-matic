use crate::blog::Blog;
use crate::gallery::Gallery;
use crate::options::Options;
use crate::serve::{serve, ServeMode};
use crate::tasks::{add_gallery_tasks, add_tasks_for_blog};
use clap::Parser;
use log::{error, LevelFilter};
use std::path::{Path, PathBuf};
use tracing::debug;

mod blog;
mod constants;
mod gallery;
mod html_writer;
mod md;
mod options;
mod serve;
mod tasks;
mod utils;

#[derive(Debug, Parser)]
#[command(author, version, about, long_about = None)]
struct Args {
    #[arg(long)]
    config: Option<PathBuf>,
    #[arg(short='o', long, value_hint = clap::ValueHint::DirPath)]
    output: PathBuf,
    #[arg(long)]
    serve: bool,
    #[arg(long, default_value_t = false)]
    public: bool,
    #[arg(long, default_value_t = false)]
    build: bool,
    #[arg(long)]
    port: Option<u16>,
    #[arg(long)]
    chroma_bin: Option<PathBuf>,
}

fn main() -> Result<(), anyhow::Error> {
    env_logger::builder()
        .filter_module("hyper", LevelFilter::Error)
        .init();
    log::set_max_level(LevelFilter::Debug);

    debug!("Starting blog-o-matic");
    debug!("Parsing args");
    let args = Args::parse();

    if args.build {
        debug!("Building blog into {:?}", args.output);
        let default_config_file = Path::new("config.toml").to_path_buf();
        let config_file = &args.config.unwrap_or(default_config_file);

        debug!("Loading config '{:?}'", config_file);

        let config = config::Config::builder()
            .add_source(config::File::from(config_file.as_path()))
            .add_source(config::Environment::with_prefix("BLOG_O_MATIC"))
            .build()?;

        debug!("Preparing options");
        let options = {
            let mut options = config.try_deserialize::<Options>()?;
            options.source_dir = config_file.parent().unwrap_or(Path::new("")).to_path_buf();
            options.output_dir = args.output.clone();
            options.chroma_bin = args.chroma_bin;
            options
        };

        debug!("Initializing blog");
        let blog = Blog::init(
            options.source_dir.join(&options.blog.source),
            options.output_dir.join(&options.blog.blog_output),
            options.output_dir.join(&options.blog.tag_output),
        )
        .map_err(|e| {
            error!("Failed to init blog: {e}");
            e
        })?;

        debug!("Initializing Gallery");
        let gallery = Gallery::new(&options).map_err(|e| {
            error!("Failed to init gallery: {e}");
            e
        })?;

        debug!("Initializing Tasks");
        let mut tasks: Vec<Box<dyn tasks::Task>> = vec![
            Box::new(tasks::DeleteDirectory::new(&options.output_dir)),
            Box::new(tasks::DirCopyTask::new(
                options.source_dir.join(&options.resources),
                options.output_dir.join("res"),
            )),
        ];

        for html_task in &options.pages {
            tasks.push(Box::new(tasks::GenHTMLTask::new(
                &html_task.title,
                options.output_dir.join(&html_task.output),
                options.source_dir.join(&html_task.input),
                &options,
            )))
        }

        add_tasks_for_blog(&blog, &options, &mut tasks);
        add_gallery_tasks(&gallery, &mut tasks);

        debug!("Executing Tasks");
        for t in &tasks {
            debug!("Executing {}", t.description());
            if let Err(e) = t.execute() {
                error!("Failed to execute task: {}", t.description());
                error!("Err: {e}");
                return Err(anyhow::anyhow!("Failed to execute task: {e}"));
            }
        }

        debug!("Build complete");
    }

    if args.serve {
        let mode = if args.public {
            ServeMode::Public
        } else {
            ServeMode::Dev
        };

        let port = args.port.unwrap_or(8000);

        debug!(
            "Beginning server mode={} port={}",
            if args.public { "public" } else { "dev" },
            port
        );

        serve(mode, port, &args.output)?;
    }

    Ok(())
}
