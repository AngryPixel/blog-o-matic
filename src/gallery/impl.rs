use crate::gallery::boiler_plate::{
    gen_child_bullet_css, gen_child_slider_css, gen_navigation_css, GALLERY_CSS_PRELUDE,
    GALLERY_HTML_AFTER_BODY, GALLERY_HTML_AFTER_TITLE, GALLERY_HTML_HEAD_TEMPLATE,
};
use crate::html_writer::with_html_writer;
use crate::md;
use crate::md::md_to_str;
use crate::options::Options;
use crate::utils::{name_to_html_folder_name, open_file_with_mk_dir};
use anyhow::anyhow;
use serde::Deserialize;
use std::io::Write;
use std::path::PathBuf;
use tracing::debug;
use uuid::Uuid;

#[derive(Debug, Deserialize)]
pub struct GalleryOptions {
    pub output: PathBuf,
    pub title: String,
    pub text: Option<PathBuf>,
}

#[derive(Debug, Deserialize)]
pub struct GalleryPage {
    pub name: String,
    pub source: PathBuf,
    pub description: String,
}

pub struct GalleryFile {
    pub source: PathBuf,
    pub output: PathBuf,
    pub id: Uuid,
    pub uri: String,
}

pub struct GalleryMetadata<'a> {
    pub gallery: &'a GalleryPage,
    pub files: Vec<GalleryFile>,
    pub output: PathBuf,
    pub uri: String,
    pub html_description: String,
}

impl<'a> GalleryMetadata<'a> {
    fn new(options: &'a Options, gallery: &'a GalleryPage) -> Result<Self, anyhow::Error> {
        let gallery_path = options.source_dir.join(&gallery.source);
        let dir = std::fs::read_dir(&gallery_path).map_err(|e| {
            anyhow!(
                "Could not read gallery dir {}:{e}",
                gallery_path.to_str().unwrap_or("")
            )
        })?;
        let gallery_output = options
            .output_dir
            .join(&options.gallery.output)
            .join(name_to_html_folder_name(&gallery.name));
        let mut files = Vec::new();

        for entry in dir {
            let entry = entry?;

            if entry.file_type()?.is_file() {
                let entry_path = entry.path();
                let Some(extension) = entry_path.extension() else {
                    return Err(anyhow!("File '{}' does not have an extension", entry_path.to_str().unwrap_or("?")));
                };

                let id = Uuid::new_v4();
                let file_output = gallery_output
                    .join(id.to_string())
                    .with_extension(extension);
                let mut uri = String::from("/");
                uri.push_str(
                    file_output
                        .strip_prefix(&options.output_dir)?
                        .to_str()
                        .ok_or(anyhow!("Failed to generate uri"))?,
                );
                files.push(GalleryFile {
                    source: entry.path(),
                    id,
                    output: file_output,
                    uri,
                })
            }
        }

        files.sort_by(|f1, f2| f2.source.cmp(&f1.source));

        let uri = format!(
            "/{}",
            gallery_output
                .strip_prefix(&options.output_dir)?
                .to_str()
                .unwrap()
        );

        let html_description = md::md_to_str(&gallery.description, &options.chroma_bin)?;
        Ok(Self {
            gallery,
            output: gallery_output,
            files,
            uri,
            html_description,
        })
    }
}

pub struct Gallery<'a> {
    pub options: &'a Options,
    pub galleries: Vec<GalleryMetadata<'a>>,
}

impl<'a> Gallery<'a> {
    pub fn new(options: &'a Options) -> Result<Self, anyhow::Error> {
        let galleries_metadata = if let Some(galleries) = &options.galleries {
            let mut galleries_metadata = Vec::with_capacity(galleries.len());
            for gallery in galleries {
                galleries_metadata.push(GalleryMetadata::new(options, gallery)?);
            }

            galleries_metadata
        } else {
            vec![]
        };

        Ok(Self {
            options,
            galleries: galleries_metadata,
        })
    }

    pub fn gen_gallery(&self, index: usize) -> Result<(), anyhow::Error> {
        let meta = &self.galleries[index];

        self.gen_gallery_css(meta)?;
        self.gen_gallery_iframe(meta)?;
        self.gen_gallery_html_page(meta)?;
        Ok(())
    }

    fn gen_gallery_css(&self, meta: &GalleryMetadata) -> Result<(), anyhow::Error> {
        debug!("Generating Gallery CSS");
        let mut file = open_file_with_mk_dir(meta.output.join("gallery.css"))?;

        file.write_all(GALLERY_CSS_PRELUDE.as_bytes())?;
        let image_count = meta.files.len();

        writeln!(file, "/* Bullets */")?;
        for i in 0..image_count {
            writeln!(file, "{}", gen_child_bullet_css(i))?;
        }

        writeln!(file, "/* Sliders */")?;
        for i in 0..image_count {
            writeln!(file, "{}", gen_child_slider_css(i))?;
        }

        writeln!(file, "/* Navigation */")?;
        for i in 0..image_count {
            writeln!(file, "{}", gen_navigation_css(i))?;
        }

        writeln!(file, "/* Images*/")?;
        for (index, gallery_file) in meta.files.iter().enumerate() {
            writeln!(
                file,
                r##"#img{index} {{ background-image: url('{}'); }}"##,
                &gallery_file.uri
            )?;
        }
        file.flush()?;
        Ok(())
    }

    fn gen_gallery_iframe(&self, meta: &GalleryMetadata) -> Result<(), anyhow::Error> {
        debug!("Generating Gallery IFrame");

        let mut file = open_file_with_mk_dir(&meta.output.join("iframe.html"))?;

        file.write_all(GALLERY_HTML_HEAD_TEMPLATE.as_bytes())?;
        write!(file, "<title>{}</title>", meta.gallery.name)?;
        file.write_all(GALLERY_HTML_AFTER_TITLE.as_bytes())?;

        for i in 0..meta.files.len() {
            writeln!(file, "<s id=\"s{}\"></s>", i)?;
        }

        writeln!(file, "<div class=\"slider\">")?;
        for i in 0..meta.files.len() {
            writeln!(file, r#"<div id="img{i}"></div>"#,)?;
        }
        writeln!(file, "</div>")?;

        if meta.files.len() > 1 {
            writeln!(file, "<div class=\"prevNext\">")?;
            for i in 0..meta.files.len() {
                let prev = if i != 0 { i - 1 } else { meta.files.len() - 1 };
                let next = if i != meta.files.len() - 1 { i + 1 } else { 0 };
                writeln!(
                    file,
                    r##"<div><a href="#s{prev}">&lt</a><a href="#s{next}">&gt</a></div>"##
                )?
            }
            writeln!(file, "</div>")?;

            writeln!(file, "<div class=\"bullets\">")?;
            for index in 0..meta.files.len() {
                writeln!(file, r##"<span>{}/{}</span>"##, index + 1, meta.files.len())?
            }
            writeln!(file, "</div>")?;
        }

        file.write_all(GALLERY_HTML_AFTER_BODY.as_bytes())?;
        file.flush()?;
        Ok(())
    }

    fn gen_gallery_html_page(&self, meta: &GalleryMetadata) -> Result<(), anyhow::Error> {
        debug!("Generating Gallery HTML");
        let mut file = open_file_with_mk_dir(meta.output.join("index.html"))?;
        let title = format!("{} Photos", &meta.gallery.name);
        with_html_writer(&mut file, self.options, Some(&title), |w| {
            w.heading(1, |w| w.text(&title))?;
            w.text(&meta.html_description)?;
            w.line_break()?;
            w.iframe(Some("gallery"), None, "./iframe.html#s0")?;

            Ok(())
        })?;
        file.flush()?;
        Ok(())
    }

    pub fn gen_gallery_index(&self) -> Result<(), anyhow::Error> {
        let mut file = open_file_with_mk_dir(
            self.options
                .output_dir
                .join(&self.options.gallery.output)
                .join("index.html"),
        )?;

        let text = if let Some(file_path) = &self.options.gallery.text {
            let file_contents = std::fs::read_to_string(self.options.source_dir.join(file_path))?;
            md_to_str(&file_contents, &None)?;
            Some(file_contents)
        } else {
            None
        };

        with_html_writer(
            &mut file,
            self.options,
            Some(&self.options.gallery.title),
            |w| {
                w.heading(1, |w| w.text(&self.options.gallery.title))?;
                if let Some(text) = text {
                    w.text(&text)?;
                }

                for meta in &self.galleries {
                    w.div(None, Some("blog_index_entry"), |w| {
                        w.heading(2, |w| {
                            w.anchor(
                                &meta.uri,
                                Some(&format!("{} Gallery", meta.gallery.name)),
                                |w| w.text(&meta.gallery.name),
                            )
                        })?;
                        w.text(&meta.html_description)
                    })?;
                }
                Ok(())
            },
        )?;
        file.flush()?;
        Ok(())
    }
}
