/*
    CSS based Gallery, inspired by
    http://stackoverflow.com/a/34696029/383904
*/
pub(super) const GALLERY_CSS_PRELUDE: &str = r#"
html, body{height:100%;}
body{font:15px/1 monospace; margin:0;}

.CSSgal{
  position: relative;
  overflow: hidden;
  height:   100%; /* Or set a fixed height */
}

/* SLIDER */
.CSSgal .slider{
  height:      100%;
  white-space: nowrap;
  font-size:   0;
  transition:  0.8s;
}

/* SLIDES */
.CSSgal .slider > *{
  font-size:       1rem;
  display:         inline-block;
  vertical-align:  top;
  height:          100%;
  width:           100%;
  background:      none 50% no-repeat;
  background-size: contain;
  background-color: black;
}

/* PREV/NEXT, CONTAINERS & ANCHORS */
.CSSgal .prevNext{
  position: absolute;
  z-index:  1;
  top:      50%;
  width:    100%;
  height:   0;
}

.CSSgal .prevNext > div+div{
  visibility: hidden; /* Hide all but first P/N container */
}
.CSSgal .prevNext a{
  background:  black;
  color:       white;
  position:    absolute;
  width:       40px;
  height:      40px;
  line-height: 40px;
  text-align:  center;
  opacity:     0.3;
  font-weight: bold;
  margin: 1em;
  text-decoration:   none;
  transform: translateY( -50% );
}
.CSSgal .prevNext a:hover{
  opacity: 0.8;
}
.CSSgal .prevNext a+a{
  right: 0px;
}

/* NAVIGATION */
.CSSgal .bullets{
  position:   absolute;
  z-index:    2;
  bottom:     0;
  padding:    10px 0;
  width:      100%;
  text-align: center;
}
.CSSgal .bullets > span{
  display:         none;
  padding:         0.5em 1em;
  height:          20px;
  line-height:     20px;
  text-decoration: none;
  text-align:      center;
  background: black;
  color:      white;
  opacity:    0.3;
}

/* ACTIVE NAVIGATION ANCHOR */
.CSSgal >s:target ~ .bullets >* { display: none; }

/* PREV/NEXT CONTAINERS VISIBILITY */
.CSSgal >s:target ~ .prevNext >* { visibility:  hidden; }
"#;

pub(super) const GALLERY_HTML_HEAD_TEMPLATE: &str = r#"
<html>
<head>
<meta charset="utf-8" />
<meta http-equiv="Content-Security-Policy" content="default-src 'self';"/>
<link rel="stylesheet" href="./gallery.css" />
"#;

pub(super) const GALLERY_HTML_AFTER_TITLE: &str = r#"<!DOCTYPE html>
</head>
<body>
<div class="CSSGal">
"#;

pub(super) const GALLERY_HTML_AFTER_BODY: &str = r#"
</div>
</body>
</html>
"#;

pub(super) fn gen_navigation_css(child: usize) -> String {
    format!(
        r#"#s{child}:target ~ .prevNext >*:nth-child({}){{ visibility: visible; }}"#,
        child + 1
    )
}

pub(super) fn gen_child_slider_css(child: usize) -> String {
    format!(
        r#"#s{child}:target ~ .slider{{ transform: translateX({}%);}}"#,
        (child as isize) * -100
    )
}

pub(super) fn gen_child_bullet_css(child: usize) -> String {
    format!(
        r#"#s{child}:target ~ .bullets >*:nth-child({}){{ display:inline-block; }}"#,
        child + 1
    )
}
