use crate::constants::{BLOG_ROOT_LINK, TAGS_ROOT_LINK};
use crate::html_writer::{with_html_writer, HTMLWriter};
use crate::md;
use crate::md::md_to_str;
use crate::options::Options;
use crate::utils::open_file_with_mk_dir;
use anyhow::anyhow;
use chrono::{Datelike, NaiveDate};
use std::collections::btree_map::Entry;
use std::collections::BTreeMap;
use std::io::{BufRead, BufReader, Read};
use std::path::{Path, PathBuf};

pub struct BlogPost {
    pub path: PathBuf,
    pub title: String,
    pub tags: Vec<String>,
    pub created: NaiveDate,
    pub last_edited: NaiveDate,
    pub first_paragraph: String,
}

impl BlogPost {
    const META_CHAR: char = '%';
    const META_KEY_TITLE: &'static str = "%title: ";
    const META_KEY_TAGS: &'static str = "%tags: ";
    const META_KEY_CREATED: &'static str = "%created: ";
    const META_KEY_LAST_EDITED: &'static str = "%last-edited: ";

    fn extract_from_file(path: impl Into<PathBuf>) -> Result<Self, anyhow::Error> {
        let filepath = path.into();
        let mut file = std::fs::OpenOptions::new()
            .read(true)
            .open(&filepath)
            .map_err(|e| anyhow!("Failed to open '{}': {e}", filepath.to_str().unwrap()))?;
        Self::extract(&mut file, filepath)
    }

    fn extract(reader: &mut dyn Read, path: PathBuf) -> Result<Self, anyhow::Error> {
        let mut buf_reader = BufReader::new(reader);

        let mut line = String::new();
        let mut title: Option<String> = None;
        let mut tags: Option<Vec<String>> = None;
        let mut created: Option<NaiveDate> = None;
        let mut last_edited: Option<NaiveDate> = None;
        loop {
            line.clear();
            buf_reader.read_line(&mut line)?;
            let line = line.trim_end_matches('\n');
            if line.is_empty() || !line.starts_with(Self::META_CHAR) {
                break;
            }

            if let Some(t) = line.strip_prefix(Self::META_KEY_TITLE) {
                title = Some(t.trim().to_string());
                continue;
            } else if let Some(date) = line.strip_prefix(Self::META_KEY_CREATED) {
                created = Some(
                    parse_date(date.trim()).map_err(|e| anyhow!("Invalid created date: {e}"))?,
                );
                continue;
            } else if let Some(date) = line.strip_prefix(Self::META_KEY_LAST_EDITED) {
                last_edited = Some(
                    parse_date(date.trim())
                        .map_err(|e| anyhow!("Invalid last-edited date: {e}"))?,
                );
                continue;
            } else if let Some(t) = line.strip_prefix(Self::META_KEY_TAGS) {
                if t.contains(' ') {
                    return Err(anyhow!("Tags can't have spaces"));
                }
                tags = Some(t.split(',').map(|x| x.to_lowercase()).collect());
                continue;
            } else {
                return Err(anyhow!("Unknown tag in line '{line}'"));
            }
        }

        // Read first non-empty line
        let mut paragraph = String::new();
        loop {
            line.clear();
            buf_reader.read_line(&mut line)?;
            let trimmed = line.trim();
            if trimmed.is_empty() {
                break;
            }
            paragraph.push_str(&line);
        }

        paragraph = md_to_str(&paragraph, &None)?;

        if title.is_none() {
            return Err(anyhow!("Missing title metadata"));
        }

        if tags.is_none() {
            return Err(anyhow!("Missing tags metadata"));
        }

        if created.is_none() {
            return Err(anyhow!("Missing created metadata"));
        }

        if last_edited.is_none() {
            return Err(anyhow!("Missing last edited metadata"));
        }

        Ok(Self {
            path,
            title: title.unwrap(),
            tags: tags.unwrap(),
            created: created.unwrap(),
            last_edited: last_edited.unwrap(),
            first_paragraph: paragraph,
        })
    }

    pub fn folder_name(&self) -> String {
        format!(
            "{}-{}",
            self.created.format("%Y-%m-%d"),
            self.title.trim().to_lowercase().replace(' ', "-")
        )
    }

    fn read_post_into_string(&self) -> Result<String, anyhow::Error> {
        let file = std::fs::OpenOptions::new()
            .read(true)
            .open(&self.path)
            .map_err(|e| anyhow!("Failed to open '{}': {e}", &self.path.to_str().unwrap()))?;
        let mut reader = BufReader::new(file);
        let mut line = String::new();
        loop {
            line.clear();
            reader.read_line(&mut line)?;
            if line.is_empty() || !line.starts_with(Self::META_CHAR) {
                break;
            }
        }
        line.clear();
        reader.read_to_string(&mut line)?;
        Ok(line)
    }

    pub fn blog_post_to_html(&self, output: &Path, options: &Options) -> Result<(), anyhow::Error> {
        let blog_post = self.read_post_into_string()?;
        let mut output = open_file_with_mk_dir(output)?;
        with_html_writer(&mut output, options, Some(&self.title), |w| {
            w.heading(1, |w| w.text(&self.title))?;
            w.div(None, Some("blog_meta"), |w| {
                w.text(&self.created.to_string())?;
                if !self.tags.is_empty() {
                    w.text(" ·")?;
                }

                for t in &self.tags {
                    w.text(" #")?;
                    w.anchor(&format!("{}/{t}", TAGS_ROOT_LINK), None, |w| w.text(t))?;
                }
                w.text(" · ")?;
                w.anchor("/", Some("Author"), |w| w.text(&options.author))?;
                Ok(())
            })?;
            md::process_markdown_str(&blog_post, w)
        })?;
        Ok(())
    }
}

fn parse_date(date: &str) -> chrono::ParseResult<NaiveDate> {
    NaiveDate::parse_from_str(date, "%Y-%m-%d")
}

pub struct Blog {
    pub posts: Vec<BlogPost>,
    pub tags: BTreeMap<String, Vec<usize>>,
    pub source_path: PathBuf,
    pub output_path: PathBuf,
    pub tags_output_path: PathBuf,
}

impl Blog {
    pub fn init(
        source_path: PathBuf,
        output_path: PathBuf,
        tags_output_path: PathBuf,
    ) -> Result<Self, anyhow::Error> {
        let dir = std::fs::read_dir(&source_path)?;

        let mut posts = Vec::new();
        let mut tags: BTreeMap<String, Vec<usize>> = BTreeMap::new();

        for entry in dir {
            let entry = entry?;

            if !entry.file_type()?.is_file() {
                continue;
            }

            let post = BlogPost::extract_from_file(entry.path())?;
            posts.push(post);
        }

        posts.sort_by(|p1, p2| p2.created.cmp(&p1.created));

        for (idx, post) in posts.iter().enumerate() {
            for tag in &post.tags {
                match tags.entry(tag.clone()) {
                    Entry::Occupied(mut o) => {
                        o.get_mut().push(idx);
                    }
                    Entry::Vacant(v) => {
                        v.insert(vec![idx]);
                    }
                }
            }
        }

        Ok(Self {
            posts,
            tags,
            source_path,
            output_path,
            tags_output_path,
        })
    }

    pub fn convert_to_output_path(&self, post: &BlogPost) -> Result<PathBuf, anyhow::Error> {
        Ok(self.output_path.join(post.folder_name()).join("index.html"))
    }

    pub fn convert_to_html_path(&self, post: &BlogPost) -> Result<String, anyhow::Error> {
        Ok(format!("{}/{}", BLOG_ROOT_LINK, post.folder_name()))
    }

    pub fn generate_index(&self, options: &Options) -> Result<(), anyhow::Error> {
        let mut output = open_file_with_mk_dir(self.output_path.join("index.html"))?;
        with_html_writer(&mut output, options, Some("Blog"), |w| {
            w.heading(1, |w| w.text("Blog"))?;

            let mut last_month = None;

            for post in &self.posts {
                let post_month = post.created.month();
                if last_month != Some(post_month) {
                    w.heading(2, |w| w.text(&post.created.format("%b %Y").to_string()))?;
                    last_month = Some(post_month);
                }
                self.write_blog_post_reference(post, w)?;
            }
            Ok(())
        })?;
        Ok(())
    }

    pub fn generate_tags_index(&self, options: &Options) -> Result<(), anyhow::Error> {
        let mut output = open_file_with_mk_dir(self.tags_output_path.join("index.html"))?;
        with_html_writer(&mut output, options, Some("Tags"), |w| {
            w.heading(1, |w| w.text("Tags"))?;

            w.list(|w| {
                for tag in self.tags.keys() {
                    w.list_item(|w| {
                        w.anchor(
                            &format!("{}/{}", TAGS_ROOT_LINK, tag),
                            Some(&format!("Tag {tag}")),
                            |w| w.text(tag),
                        )
                    })?;
                }
                Ok(())
            })
        })?;
        Ok(())
    }

    pub fn generate_tag_page(&self, options: &Options, tag: &str) -> Result<(), anyhow::Error> {
        let mut output = open_file_with_mk_dir(self.tags_output_path.join(tag).join("index.html"))?;
        let title = format!("Posts with {tag}");
        with_html_writer(&mut output, options, Some(&title), |w| {
            w.heading(1, |w| w.text(&title))?;

            let post_indices = self.tags.get(tag).ok_or(anyhow!("No such tag {tag}"))?;
            for index in post_indices {
                let post = self.posts.get(*index).ok_or(anyhow!("No such post with"))?;
                self.write_blog_post_reference(post, w)?;
            }
            Ok(())
        })?;
        Ok(())
    }

    fn write_blog_post_reference(
        &self,
        post: &BlogPost,
        w: &mut HTMLWriter,
    ) -> Result<(), anyhow::Error> {
        let blog_post_file = self.convert_to_html_path(post)?;
        w.div(None, Some("blog_index_entry"), |w| {
            w.heading(2, |w| {
                w.anchor(&blog_post_file, Some(&post.title), |w| w.text(&post.title))
            })?;
            w.div(None, Some("blog_meta"), |w| {
                w.text(&post.created.to_string())?;
                for t in &post.tags {
                    w.text(" #")?;
                    w.anchor(&format!("{}/{t}", TAGS_ROOT_LINK), None, |w| w.text(t))?;
                }
                Ok(())
            })?;
            w.text(&post.first_paragraph)
        })
    }
}

#[test]
fn test_parse_blog_post_medata() {
    let text = r#"%title: This is blog
%tags: foo,bar,x-y-z
%created: 1980-05-19
%last-edited: 1980-05-20

 Something else
line two

line three
"#;

    let mut reader = std::io::Cursor::new(text.as_bytes());
    let post = BlogPost::extract(&mut reader, PathBuf::from("/foo")).unwrap();
    assert_eq!(post.title, "This is blog");
    assert_eq!(post.path, PathBuf::from("/foo"));
    assert_eq!(
        post.tags,
        vec!["foo".to_string(), "bar".to_string(), "x-y-z".to_string()]
    );
    assert_eq!(post.created, NaiveDate::from_ymd_opt(1980, 05, 19).unwrap());
    assert_eq!(
        post.last_edited,
        NaiveDate::from_ymd_opt(1980, 05, 20).unwrap()
    );
    assert_eq!(post.first_paragraph, "<p>Something else\nline two</p>\n")
}
