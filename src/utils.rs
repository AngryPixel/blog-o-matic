use log::debug;
use std::fs::File;
use std::io::BufWriter;
use std::path::Path;

pub fn open_file_with_mk_dir(path: impl AsRef<Path>) -> std::io::Result<BufWriter<File>> {
    if let Some(parent) = path.as_ref().parent() {
        debug!("creating parent dir");
        std::fs::create_dir_all(parent)?;
    };

    debug!("Opening file {:?}", path.as_ref());

    std::fs::OpenOptions::new()
        .write(true)
        .truncate(true)
        .create(true)
        .open(path)
        .map(BufWriter::new)
}

pub fn name_to_html_folder_name(name: &str) -> String {
    name.trim().to_lowercase().replace(' ', "-")
}
