use crate::tasks::Task;
use anyhow::{anyhow, Error};
use log::error;
use std::path::{Path, PathBuf};

pub struct FileCopyTask {
    source: PathBuf,
    destination: PathBuf,
}

impl FileCopyTask {
    #[allow(unused)]
    pub fn new<T: AsRef<Path>>(source: T, destination: T) -> Self {
        Self {
            source: source.as_ref().to_path_buf(),
            destination: destination.as_ref().to_path_buf(),
        }
    }
}

impl Task for FileCopyTask {
    fn description(&self) -> &str {
        "File Copy"
    }

    fn execute(&self) -> Result<(), Error> {
        if let Err(e) = copy_file_and_create_parent(&self.source, &self.destination) {
            error!(
                "FileCopy> Failed to copy file '{}' to '{}': {e}",
                self.source.to_str().unwrap(),
                self.destination.to_str().unwrap()
            )
        }
        Ok(())
    }
}

pub struct DirCopyTask {
    source: PathBuf,
    destination: PathBuf,
}

impl DirCopyTask {
    pub fn new<T: AsRef<Path>>(source: T, destination: T) -> Self {
        Self {
            source: source.as_ref().to_path_buf(),
            destination: destination.as_ref().to_path_buf(),
        }
    }
}

impl Task for DirCopyTask {
    fn description(&self) -> &str {
        "Dir Copy"
    }

    fn execute(&self) -> Result<(), Error> {
        let mut dirs = vec![self.source.clone()];

        while let Some(current_dir) = dirs.pop() {
            let dir = match std::fs::read_dir(&current_dir) {
                Ok(d) => d,
                Err(e) => {
                    error!(
                        "DirCopy> Failed to read directory '{}': {e}",
                        self.source.to_str().unwrap()
                    );
                    return Err(e.into());
                }
            };

            for entry in dir {
                let entry = entry?;
                let file_type = entry.file_type()?;
                if file_type.is_file() {
                    let entry_path = entry.path();
                    let destination_path = self
                        .destination
                        .join(entry_path.strip_prefix(&self.source)?);
                    if let Err(e) = copy_file_and_create_parent(&entry_path, &destination_path) {
                        error!(
                            "DirCopy> Failed to copy file '{}' to '{}': {e}",
                            entry_path.to_str().unwrap(),
                            destination_path.to_str().unwrap()
                        )
                    }
                } else if file_type.is_dir() {
                    dirs.push(entry.path());
                }
            }
        }

        Ok(())
    }
}

fn copy_file_and_create_parent(source: &Path, dst: &Path) -> std::io::Result<u64> {
    if let Some(parent) = dst.parent() {
        std::fs::create_dir_all(parent)?;
    };

    std::fs::copy(source, dst)
}

pub struct DeleteDirectory {
    dir: PathBuf,
}

impl DeleteDirectory {
    pub fn new(dir: &Path) -> Self {
        Self {
            dir: dir.to_path_buf(),
        }
    }
}

impl Task for DeleteDirectory {
    fn description(&self) -> &str {
        "Dir Delete"
    }

    fn execute(&self) -> Result<(), Error> {
        if let Err(e) = std::fs::remove_dir_all(&self.dir) {
            if e.kind() == std::io::ErrorKind::NotFound {
                return Ok(());
            }
            error!(
                "DeleteDir> Failed to delete '{}': {e}",
                self.dir.to_str().unwrap()
            );
            return Err(anyhow!(e));
        }
        Ok(())
    }
}
