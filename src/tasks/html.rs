use crate::md::md_to_html_with_file;
use crate::options::Options;
use crate::tasks::Task;
use anyhow::Error;
use std::path::PathBuf;

pub struct GenHTMLTask<'a> {
    title: String,
    output: PathBuf,
    body: PathBuf,
    options: &'a Options,
    desc: String,
}

impl<'a> GenHTMLTask<'a> {
    pub fn new(
        title: impl Into<String>,
        output: PathBuf,
        body: PathBuf,
        options: &'a Options,
    ) -> Self {
        let desc = format!("Gen HTML '{}'", &body.to_str().unwrap());
        Self {
            title: title.into(),
            output,
            body,
            options,
            desc,
        }
    }
}

impl<'a> Task for GenHTMLTask<'a> {
    fn description(&self) -> &str {
        &self.desc
    }

    fn execute(&self) -> Result<(), Error> {
        md_to_html_with_file(&self.body, &self.title, &self.output, self.options)
    }
}
