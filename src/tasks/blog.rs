use crate::blog::Blog;
use crate::options::Options;
use crate::tasks::rss::GenRSS;
use crate::tasks::Task;
use anyhow::Error;

pub fn add_tasks_for_blog<'a>(
    blog: &'a Blog,
    options: &'a Options,
    tasks: &mut Vec<Box<dyn Task + 'a>>,
) {
    // Generate Blog Index
    tasks.push(Box::new(GenBlogIndex::new(blog, options)));

    // Generate Tags Index
    tasks.push(Box::new(GenBlogTags::new(blog, options)));

    // Generate Blog Tags
    for tag in blog.tags.keys() {
        tasks.push(Box::new(GenTagHTML::new(blog, tag, options)));
    }

    // Generate RSS
    tasks.push(Box::new(GenRSS::new(
        blog,
        options,
        options.output_dir.join("rss").join("rss.xml"),
    )));

    // Generate posts:
    for i in 0..blog.posts.len() {
        tasks.push(Box::new(GenBlogPostHTML::new(blog, i, options)));
    }
}

pub struct GenBlogPostHTML<'a> {
    desc: String,
    blog: &'a Blog,
    post_index: usize,
    options: &'a Options,
}

impl<'a> GenBlogPostHTML<'a> {
    pub fn new(blog: &'a Blog, post_index: usize, options: &'a Options) -> Self {
        Self {
            blog,
            post_index,
            options,
            desc: format!(
                "Gen Blog Post '{}",
                blog.posts[post_index].path.to_str().unwrap()
            ),
        }
    }
}

impl<'a> Task for GenBlogPostHTML<'a> {
    fn description(&self) -> &str {
        &self.desc
    }

    fn execute(&self) -> Result<(), Error> {
        let blog_post = &self.blog.posts[self.post_index];
        let output_path = self.blog.convert_to_output_path(blog_post)?;
        blog_post.blog_post_to_html(&output_path, self.options)
    }
}

pub struct GenBlogIndex<'a> {
    blog: &'a Blog,
    options: &'a Options,
}

impl<'a> GenBlogIndex<'a> {
    pub fn new(blog: &'a Blog, options: &'a Options) -> Self {
        Self { blog, options }
    }
}

impl<'a> Task for GenBlogIndex<'a> {
    fn description(&self) -> &str {
        "Gen Blog Index"
    }

    fn execute(&self) -> Result<(), Error> {
        self.blog.generate_index(self.options)
    }
}

pub struct GenBlogTags<'a> {
    blog: &'a Blog,
    options: &'a Options,
}

impl<'a> GenBlogTags<'a> {
    pub fn new(blog: &'a Blog, options: &'a Options) -> Self {
        Self { blog, options }
    }
}

impl<'a> Task for GenBlogTags<'a> {
    fn description(&self) -> &str {
        "Gen Blog Tags Index"
    }

    fn execute(&self) -> Result<(), Error> {
        self.blog.generate_tags_index(self.options)
    }
}

pub struct GenTagHTML<'a> {
    blog: &'a Blog,
    tag: String,
    options: &'a Options,
    desc: String,
}

impl<'a> GenTagHTML<'a> {
    pub fn new(blog: &'a Blog, tag: &str, options: &'a Options) -> Self {
        Self {
            blog,
            tag: tag.to_string(),
            options,
            desc: format!("Gen Tag '{tag}' page"),
        }
    }
}

impl<'a> Task for GenTagHTML<'a> {
    fn description(&self) -> &str {
        &self.desc
    }

    fn execute(&self) -> Result<(), Error> {
        self.blog.generate_tag_page(self.options, &self.tag)
    }
}
