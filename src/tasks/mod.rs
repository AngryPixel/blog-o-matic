mod blog;
mod files;
mod gallery;
mod html;
mod rss;

pub use blog::*;
pub use files::*;
pub use gallery::*;
pub use html::*;

pub trait Task: Send {
    fn description(&self) -> &str;
    fn execute(&self) -> Result<(), anyhow::Error>;
}
