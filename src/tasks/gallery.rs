use crate::gallery::Gallery;
use crate::tasks::{FileCopyTask, Task};
use anyhow::Error;

pub fn add_gallery_tasks<'a>(gallery: &'a Gallery, tasks: &mut Vec<Box<dyn Task + 'a>>) {
    // Gallery Pages
    for (i, meta) in gallery.galleries.iter().enumerate() {
        tasks.push(Box::new(GenGalleryPage::new(gallery, i)));
        for file in &meta.files {
            tasks.push(Box::new(FileCopyTask::new(&file.source, &file.output)));
        }
    }
    // Gallery Index
    tasks.push(Box::new(GenGalleryIndex::new(gallery)));
}

struct GenGalleryPage<'a> {
    gallery: &'a Gallery<'a>,
    page_index: usize,
    desc: String,
}

impl<'a> GenGalleryPage<'a> {
    fn new(gallery: &'a Gallery, index: usize) -> Self {
        Self {
            gallery,
            page_index: index,
            desc: format!("Gen Gallery {}", gallery.galleries[index].gallery.name),
        }
    }
}

impl<'a> Task for GenGalleryPage<'a> {
    fn description(&self) -> &str {
        &self.desc
    }

    fn execute(&self) -> Result<(), Error> {
        self.gallery.gen_gallery(self.page_index)
    }
}

struct GenGalleryIndex<'a> {
    gallery: &'a Gallery<'a>,
}

impl<'a> GenGalleryIndex<'a> {
    fn new(gallery: &'a Gallery) -> Self {
        Self { gallery }
    }
}

impl<'a> Task for GenGalleryIndex<'a> {
    fn description(&self) -> &str {
        "Gen Gallery Index"
    }

    fn execute(&self) -> Result<(), Error> {
        self.gallery.gen_gallery_index()
    }
}
