use crate::blog::Blog;
use crate::options::Options;
use crate::tasks::Task;
use crate::utils::open_file_with_mk_dir;
use anyhow::Error;
use log::debug;
use std::io::BufReader;
use std::path::PathBuf;
use std::str::FromStr;

pub struct GenRSS<'a> {
    blog: &'a Blog,
    options: &'a Options,
    output: PathBuf,
}

impl<'a> GenRSS<'a> {
    pub fn new(blog: &'a Blog, options: &'a Options, output: PathBuf) -> Self {
        Self {
            blog,
            options,
            output,
        }
    }
}

impl<'a> Task for GenRSS<'a> {
    fn description(&self) -> &str {
        "Gen RSS"
    }

    fn execute(&self) -> Result<(), Error> {
        use atom_syndication as feed;

        let rss_opt = &self.options.rss;

        let feed_author = vec![feed::PersonBuilder::default()
            .name(self.options.author.clone())
            .uri(Some(self.options.domain.clone()))
            .build()];

        let categories = rss_opt
            .categories
            .iter()
            .map(|c| feed::CategoryBuilder::default().term(c).build())
            .collect::<Vec<_>>();

        let entries = self
            .blog
            .posts
            .iter()
            .map(|post| {
                let categories = post
                    .tags
                    .iter()
                    .map(|c| feed::CategoryBuilder::default().term(c).build())
                    .collect::<Vec<_>>();

                let post_link = format!(
                    "{}{}",
                    self.options.domain,
                    self.blog.convert_to_html_path(post).unwrap()
                );

                feed::EntryBuilder::default()
                    .title(post.title.clone())
                    .published(
                        feed::FixedDateTime::from_str(&format!(
                            "{}T00:00:00Z",
                            post.created.format("%Y-%m-%d")
                        ))
                        .unwrap(),
                    )
                    .updated(
                        feed::FixedDateTime::from_str(&format!(
                            "{}T00:00:00Z",
                            post.last_edited.format("%Y-%m-%d")
                        ))
                        .unwrap(),
                    )
                    .link(
                        feed::LinkBuilder::default()
                            .rel("alternate")
                            .href(post_link.clone())
                            .build(),
                    )
                    .summary(
                        feed::TextBuilder::default()
                            .r#type(feed::TextType::Html)
                            .value(&post.first_paragraph)
                            .build(),
                    )
                    .id(post_link)
                    .categories(categories)
                    .build()
            })
            .collect::<Vec<_>>();

        let channel = feed::FeedBuilder::default()
            .title(rss_opt.title.clone())
            .link(
                feed::LinkBuilder::default()
                    .href(self.options.domain.clone())
                    .build(),
            )
            .link(
                feed::LinkBuilder::default()
                    .rel("self".to_string())
                    .href(format!("{}/index.html", self.options.domain.clone()))
                    .build(),
            )
            .authors(feed_author)
            .categories(categories)
            .updated(chrono::Utc::now().fixed_offset())
            .subtitle(Some(
                feed::TextBuilder::default()
                    .r#type(feed::TextType::Text)
                    .value(rss_opt.description.clone())
                    .build(),
            ))
            .entries(entries)
            .build();

        {
            debug!("Generating RSS");
            let output = open_file_with_mk_dir(&self.output)?;
            channel.write_to(output)?;
        }

        {
            debug!("Checking RSS");
            // Ensure we can still read the rss file afterwards since the write does not escape
            // xml characters.
            let file = std::fs::OpenOptions::new().read(true).open(&self.output)?;
            let _ = feed::Feed::read_from(BufReader::new(file))?;
        }

        Ok(())
    }
}
