use crate::options::Options;
use anyhow::anyhow;
use log::error;
use std::collections::hash_map::Entry;
use std::collections::HashMap;
use std::io::Write;
use std::path::{Path, PathBuf};

const MAX_HEADER_LEVELS: usize = 5;

pub struct HTMLWriter<'a, 'b> {
    footnote_counter: usize,
    footnote_id_to_counter: HashMap<String, usize>,
    writer: &'a mut dyn Write,
    heading_counters: [u32; MAX_HEADER_LEVELS],
    chroma_bin: &'b Option<PathBuf>,
}

pub type HTMLWriterResult = Result<(), anyhow::Error>;

pub trait WriterFn: FnOnce(&mut HTMLWriter) -> HTMLWriterResult {}
impl<T: FnOnce(&mut HTMLWriter) -> HTMLWriterResult> WriterFn for T {}

impl<'a, 'b> HTMLWriter<'a, 'b> {
    pub fn new(writer: &'a mut dyn Write, chroma_bin: &'b Option<PathBuf>) -> Self {
        Self {
            footnote_counter: 0,
            footnote_id_to_counter: HashMap::new(),
            writer,
            heading_counters: [0u32; MAX_HEADER_LEVELS],
            chroma_bin,
        }
    }

    pub fn block_quote<F: WriterFn>(&mut self, f: F) -> HTMLWriterResult {
        writeln!(self.writer, "<blockquote>")?;
        f(self)?;
        writeln!(self.writer, "</blockquote>")?;
        Ok(())
    }

    pub fn footnote_definition<F: WriterFn>(
        &mut self,
        footnote_id: &str,
        f: F,
    ) -> HTMLWriterResult {
        let Some(footnote_index) = self.footnote_id_to_counter.get(footnote_id) else {
            return Err(anyhow!("Footnote id '{footnote_id}' but never used"))
        };

        let index_str = footnote_index.to_string();
        writeln!(self.writer, "<div id=\"{footnote_id}\" class=\"footnote\">")?;
        self.text(&format!("<strong>[{index_str}]:</strong> "))?;
        f(self)?;
        writeln!(self.writer, "</div>")?;
        Ok(())
    }

    pub fn footnote_ref(&mut self, footnote_id: &str) -> HTMLWriterResult {
        let index = match self.footnote_id_to_counter.entry(footnote_id.to_string()) {
            Entry::Occupied(o) => *o.get(),
            Entry::Vacant(v) => {
                self.footnote_counter += 1;
                v.insert(self.footnote_counter);
                self.footnote_counter
            }
        };

        self.anchor(&format!("#{footnote_id}"), None, |w| {
            w.text(&format!("[{index}]"))
        })
    }

    pub fn div<F: WriterFn>(
        &mut self,
        id: Option<&str>,
        class: Option<&str>,
        f: F,
    ) -> HTMLWriterResult {
        write!(self.writer, "<div")?;
        if let Some(id) = id {
            write!(self.writer, " id=\"{id}\"")?;
        }
        if let Some(class) = class {
            write!(self.writer, " class=\"{class}\"")?;
        }
        writeln!(self.writer, ">")?;
        f(self)?;
        writeln!(self.writer, "</div>")?;
        Ok(())
    }

    pub fn anchor<F: WriterFn>(
        &mut self,
        link: &str,
        title: Option<&str>,
        f: F,
    ) -> HTMLWriterResult {
        if let Some(title) = title {
            write!(self.writer, r#"<a href="{link}" title="{title}">"#)?;
        } else {
            write!(self.writer, r#"<a href="{link}">"#)?;
        }
        f(self)?;
        write!(self.writer, "</a>")?;
        Ok(())
    }

    pub fn html(&mut self, link: &str) -> HTMLWriterResult {
        write!(self.writer, r#"<a href="{link}">{link}</a>"#)?;
        Ok(())
    }

    pub fn text(&mut self, text: &str) -> HTMLWriterResult {
        write!(self.writer, "{text}")?;
        Ok(())
    }

    pub fn list<F: WriterFn>(&mut self, f: F) -> HTMLWriterResult {
        writeln!(self.writer, "<ul>")?;
        f(self)?;
        writeln!(self.writer, "</ul>")?;
        Ok(())
    }

    pub fn line_break(&mut self) -> HTMLWriterResult {
        writeln!(self.writer, "<br>")?;
        Ok(())
    }

    pub fn inline_code(&mut self, value: &str) -> HTMLWriterResult {
        write!(
            self.writer,
            "<code class=\"inline_code chroma\">{value}</code>"
        )?;
        Ok(())
    }

    pub fn strikethrough<F: WriterFn>(&mut self, f: F) -> HTMLWriterResult {
        write!(self.writer, "<s>")?;
        f(self)?;
        write!(self.writer, "</s>")?;
        Ok(())
    }

    pub fn emphasis<F: WriterFn>(&mut self, f: F) -> HTMLWriterResult {
        write!(self.writer, "<em>")?;
        f(self)?;
        write!(self.writer, "</em>")?;
        Ok(())
    }

    pub fn strong<F: WriterFn>(&mut self, f: F) -> HTMLWriterResult {
        write!(self.writer, "<strong>")?;
        f(self)?;
        write!(self.writer, "</strong>")?;
        Ok(())
    }

    pub fn image(&mut self, url: &str, alt: &str, title: Option<&str>) -> HTMLWriterResult {
        let title = if let Some(title) = title {
            format!(r#" title="{title}"#)
        } else {
            String::new()
        };
        write!(self.writer, r#"<img url="{url}" alt="{alt}"{title}/>"#)?;
        Ok(())
    }

    pub fn heading<F: WriterFn>(&mut self, level: u8, f: F) -> HTMLWriterResult {
        let generate_local_link = level >= 2;
        if generate_local_link {
            let heading_index = level - 2;
            let heading_id = self.gen_heading_id(heading_index as usize);
            writeln!(
                self.writer,
                r##"<div id="{heading_id}" class="heading_anchor"></div>"##
            )?;
            write!(self.writer, r##"<h{level} class="heading_link">"##)?;
            let heading_link = format!("#{heading_id}");
            self.anchor(&heading_link, None, |w| f(w))?;
        } else {
            write!(self.writer, "<h{level}>")?;
            f(self)?;
        }
        writeln!(self.writer, "</h{level}>")?;
        Ok(())
    }

    fn gen_heading_id(&mut self, heading_index: usize) -> String {
        self.heading_counters[heading_index] += 1;
        for i in heading_index + 1..MAX_HEADER_LEVELS {
            self.heading_counters[i] = 0;
        }

        let mut id = String::from("sec");
        for value in &self.heading_counters {
            if *value == 0 {
                break;
            }

            id.push_str(&value.to_string())
        }

        id
    }

    pub fn list_item<F: WriterFn>(&mut self, f: F) -> HTMLWriterResult {
        write!(self.writer, "<li>")?;
        f(self)?;
        writeln!(self.writer, "</li>")?;
        Ok(())
    }

    pub fn thematic_break(&mut self) -> HTMLWriterResult {
        writeln!(self.writer, "<hr />")?;
        Ok(())
    }

    pub fn paragraph<F: WriterFn>(&mut self, f: F) -> HTMLWriterResult {
        write!(self.writer, "<p>")?;
        f(self)?;
        writeln!(self.writer, "</p>")?;
        Ok(())
    }

    pub fn code(&mut self, code: &str, lang: Option<&str>, _: Option<&str>) -> HTMLWriterResult {
        let highlighted_code = crate::md::highlight_code(code, &lang, self.chroma_bin)?;
        write!(self.writer, "{highlighted_code}")?;
        Ok(())
    }

    pub fn iframe(
        &mut self,
        class: Option<&str>,
        sandbox: Option<&str>,
        url: &str,
    ) -> HTMLWriterResult {
        write!(self.writer, "<iframe ")?;
        if let Some(class) = class {
            write!(self.writer, r#" class="{class}""#)?;
        }
        write!(self.writer, " sandbox")?;
        if let Some(sandbox) = sandbox {
            write!(self.writer, r#"="{sandbox}""#)?;
        }
        write!(self.writer, r#" src="{url}">"#)?;
        writeln!(self.writer, "</iframe>")?;
        Ok(())
    }
}

const HTML_START: &str = r#"<!DOCTYPE html>
<html lang="en">
<head>"#;

const HTML_BODY_START: &str = r#"</head>
<body>"#;

const HTML_END: &str = r#"</body>
</html>"#;

pub fn with_html_writer<F: WriterFn>(
    output: &mut dyn Write,
    options: &Options,
    title: Option<&str>,
    f: F,
) -> HTMLWriterResult {
    fn read_optional_path(
        source_dir: &Path,
        p: &Option<PathBuf>,
    ) -> Result<Option<String>, std::io::Error> {
        if let Some(p) = p {
            match std::fs::read_to_string(source_dir.join(p)) {
                Ok(s) => Ok(Some(s)),
                Err(e) => {
                    error!(
                        "Failed to read html options file '{}': {e}",
                        p.to_str().unwrap()
                    );
                    Err(e)
                }
            }
        } else {
            Ok(None)
        }
    }

    let header = read_optional_path(&options.source_dir, &options.html.header)?;
    let nav = read_optional_path(&options.source_dir, &options.html.nav)?;
    let footer = read_optional_path(&options.source_dir, &options.html.footer)?;

    // Start Html
    output.write_all(HTML_START.as_bytes())?;

    // Write header
    if let Some(h) = header {
        output.write_all(h.as_bytes())?;
    }

    // Write title
    if let Some(title) = title {
        output.write_all(format!("<title>{}</title>\n", title).as_bytes())?;
    }

    // Start Body
    output.write_all(HTML_BODY_START.as_bytes())?;

    // Write nav
    if let Some(n) = nav {
        output.write_all(n.as_bytes())?;
    }

    let mut html_output = HTMLWriter::new(output, &options.chroma_bin);

    f(&mut html_output)?;

    // Write footer
    if let Some(f) = footer {
        output.write_all(f.as_bytes())?;
    }

    // Close Html
    output.write_all(HTML_END.as_bytes())?;

    output.flush()?;

    Ok(())
}
