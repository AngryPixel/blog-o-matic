use crate::html_writer::HTMLWriter;
use log::debug;
use markdown::mdast::Node;
use std::path::{Path, PathBuf};

#[allow(unused)]
pub fn process_markdown(path: &Path, output: &mut HTMLWriter) -> Result<(), anyhow::Error> {
    let file_contents = std::fs::read_to_string(path)?;

    process_markdown_str(&file_contents, output)
}

pub fn md_to_str(md: &str, chroma_bin: &Option<PathBuf>) -> Result<String, anyhow::Error> {
    let mut vec = Vec::<u8>::new();
    let mut writer = HTMLWriter::new(&mut vec, chroma_bin);
    process_markdown_str(md, &mut writer)?;
    Ok(String::from_utf8_lossy(&vec).to_string())
}

pub fn process_markdown_str(
    file_contents: &str,
    output: &mut HTMLWriter,
) -> Result<(), anyhow::Error> {
    let options = markdown::ParseOptions::gfm();

    debug!("Parsing markdown");
    let ast = markdown::to_mdast(file_contents, &options).map_err(|e| anyhow::anyhow!(e))?;

    match ast {
        Node::Root(r) => {
            for child in &r.children {
                handle_ast_node(child, output)?;
            }
        }
        _ => return Err(anyhow::anyhow!("Expected root element")),
    }

    Ok(())
}

fn handle_ast_nodes(nodes: &[Node], writer: &mut HTMLWriter) -> Result<(), anyhow::Error> {
    for child in nodes {
        handle_ast_node(child, writer)?;
    }
    Ok(())
}

fn handle_ast_node(node: &Node, writer: &mut HTMLWriter) -> Result<(), anyhow::Error> {
    match node {
        Node::Root(_) => Err(anyhow::anyhow!("Unexpected root node")),
        Node::BlockQuote(bq) => writer
            .block_quote(|w| -> Result<(), anyhow::Error> { handle_ast_nodes(&bq.children, w) }),
        Node::FootnoteDefinition(def) => {
            writer.footnote_definition(&def.identifier, |w| handle_ast_nodes(&def.children, w))
        }
        Node::List(li) => writer.list(|w| handle_ast_nodes(&li.children, w)),
        Node::Break(_) => writer.line_break(),
        Node::InlineCode(c) => writer.inline_code(&c.value),
        Node::Delete(d) => writer.strikethrough(|w| handle_ast_nodes(&d.children, w)),
        Node::Emphasis(em) => writer.emphasis(|w| handle_ast_nodes(&em.children, w)),
        Node::FootnoteReference(r) => writer.footnote_ref(&r.identifier),
        Node::Image(img) => writer.image(&img.url, &img.alt, img.title.as_deref()),
        Node::ImageReference(_) => Err(anyhow::anyhow!("Image Reference not implemented")),
        Node::Link(link) => writer.anchor(&link.url, link.title.as_deref(), |w| {
            handle_ast_nodes(&link.children, w)
        }),
        Node::LinkReference(_) => Err(anyhow::anyhow!("Link Reference not implemented")),
        Node::Strong(strong) => writer.strong(|w| handle_ast_nodes(&strong.children, w)),
        Node::Text(t) => writer.text(&t.value),
        Node::Code(c) => {
            let lang = c.lang.as_deref();
            let meta = c.meta.as_deref();
            writer.code(&c.value, lang, meta)
        }
        Node::Heading(h) => writer.heading(h.depth, |w| handle_ast_nodes(&h.children, w)),
        Node::Table(_) => Err(anyhow::anyhow!("Tables no implemented")),
        Node::ThematicBreak(_) => writer.thematic_break(),
        Node::TableRow(_) => Err(anyhow::anyhow!("Tables no implemented")),
        Node::TableCell(_) => Err(anyhow::anyhow!("Tables no implemented")),
        Node::ListItem(li) => writer.list_item(|w| handle_ast_nodes(&li.children, w)),
        Node::Definition(_) => Err(anyhow::anyhow!("Definitions not implemented")),
        Node::Paragraph(p) => writer.paragraph(|w| handle_ast_nodes(&p.children, w)),
        Node::Html(h) => writer.html(&h.value),
        _ => Err(anyhow::anyhow!("Unhandled node : {:?}", node)),
    }
}
