use crate::html_writer::with_html_writer;
use crate::md::process::process_markdown_str;
use crate::options::Options;
use crate::utils::open_file_with_mk_dir;
use std::path::Path;
use tracing::debug;

pub fn md_to_html_with_file(
    input: &Path,
    title: &str,
    output_path: &Path,
    options: &Options,
) -> Result<(), anyhow::Error> {
    debug!("Reading contents of {:?}", input);
    let contents = std::fs::read_to_string(input)?;
    md_to_html(&contents, title, output_path, options)
}

pub fn md_to_html(
    md: &str,
    title: &str,
    output_path: &Path,
    options: &Options,
) -> Result<(), anyhow::Error> {
    if let Some(parent) = output_path.parent() {
        debug!("Creating folder path for {:?}", parent);
        std::fs::create_dir_all(parent)?;
    };

    debug!("Opening output {:?}", output_path);
    let mut output = open_file_with_mk_dir(output_path)?;

    with_html_writer(&mut output, options, Some(title), |w| {
        process_markdown_str(md, w)
    })?;

    Ok(())
}
