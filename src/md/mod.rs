mod code_highlight;
mod html;
mod process;

pub use code_highlight::highlight_code;
pub use html::{md_to_html, md_to_html_with_file};
pub use process::{md_to_str, process_markdown_str};
