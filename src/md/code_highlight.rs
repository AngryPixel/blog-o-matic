use anyhow::anyhow;
use log::debug;
use std::io::Write;
use std::path::PathBuf;
use std::process::Stdio;

pub fn highlight_code(
    code: &str,
    lang: &Option<&str>,
    chroma_bin: &Option<PathBuf>,
) -> Result<String, anyhow::Error> {
    debug!("Executing code highlight (chroma={:?})", chroma_bin);
    let mut cmd = if let Some(path) = chroma_bin {
        std::process::Command::new(path)
    } else {
        std::process::Command::new("chroma")
    };
    cmd.arg("--html").arg("--html-only");
    if let Some(lang) = lang {
        cmd.arg("-l").arg(lang);
    }

    let mut process = cmd.stdin(Stdio::piped()).stdout(Stdio::piped()).spawn()?;

    {
        let mut stdin = process.stdin.take().unwrap();
        write!(stdin, "{code}")?;
    }

    if !process.wait()?.success() {
        return Err(anyhow!("Failed to execute code highlighting process"));
    }

    let html = std::io::read_to_string(process.stdout.take().unwrap())?;

    Ok(html)
}
