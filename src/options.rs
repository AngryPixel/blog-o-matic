use crate::gallery::{GalleryOptions, GalleryPage};
use serde::Deserialize;
use std::path::PathBuf;

#[derive(Debug, Deserialize)]
pub struct Options {
    #[serde(skip)]
    pub output_dir: PathBuf,
    pub resources: PathBuf,
    pub html: HtmlOptions,
    pub rss: RssOptions,
    pub blog: BlogOptions,
    pub pages: Vec<GenHTML>,
    pub author: String,
    #[serde(skip)]
    pub source_dir: PathBuf,
    pub domain: String,
    pub gallery: GalleryOptions,
    pub galleries: Option<Vec<GalleryPage>>,
    #[serde(skip)]
    pub chroma_bin: Option<PathBuf>,
}

#[derive(Debug, Deserialize)]
pub struct HtmlOptions {
    pub header: Option<PathBuf>,
    pub nav: Option<PathBuf>,
    pub footer: Option<PathBuf>,
}

#[derive(Debug, Deserialize)]
pub struct BlogOptions {
    pub source: PathBuf,
    pub blog_output: PathBuf,
    pub tag_output: PathBuf,
}

#[derive(Debug, Deserialize)]
pub struct RssOptions {
    pub categories: Vec<String>,
    pub title: String,
    pub copyright: String,
    pub language: String,
    pub description: String,
}

#[derive(Debug, Deserialize)]
pub struct GenHTML {
    pub input: PathBuf,
    pub output: PathBuf,
    pub title: String,
}
