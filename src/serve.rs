use anyhow::anyhow;
use axum::routing::get;
use axum::Router;
use std::net::SocketAddr;
use std::path::Path;
use tower_http::services::{ServeDir, ServeFile};
use tower_http::trace::TraceLayer;

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum ServeMode {
    Public,
    Dev,
}
pub fn serve(mode: ServeMode, port: u16, web_dir: &Path) -> Result<(), anyhow::Error> {
    let listen_ip = if mode == ServeMode::Dev {
        [127, 0, 0, 1]
    } else {
        [0, 0, 0, 0]
    };
    let addr = SocketAddr::from((listen_ip, port));

    let runtime = tokio::runtime::Builder::new_current_thread()
        .enable_all()
        .build()
        .map_err(|e| anyhow!("Failed to build runtime: {e}"))?;
    runtime.block_on(async move {
        tokio::select! {
            s = tokio::signal::ctrl_c() => {
                s.map_err(|e| anyhow!("Failed to wait for signal:{e}"))
            }

            v = run(&addr, web_dir) => {
                v
            }
        }
    })?;
    Ok(())
}

async fn run(addr: &SocketAddr, web_dir: &Path) -> Result<(), anyhow::Error> {
    let router = serve_dir(web_dir);
    axum::Server::bind(addr)
        .serve(router.layer(TraceLayer::new_for_http()).into_make_service())
        .await
        .map_err(|e| anyhow!("Failed to server:{e}"))?;

    Ok(())
}

fn serve_dir(dir: &Path) -> Router {
    let serve_dir = ServeDir::new(dir).not_found_service(ServeFile::new(dir.join("404.html")));
    Router::new()
        .route("/health_check", get(health_check))
        .nest_service("/", serve_dir)
}

async fn health_check() -> &'static str {
    "I'm still alive"
}
